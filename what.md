# Game Programming Patterns

This talk was going to be about me showing how to implement different patterns.

But that wouldn't be too much fun.

First, because I'm not a game programmer, and programming this stuff is _real hard_.

But also because, as a newcomer, I have still a lot to learn.

So I figured I could look at what others have done as well.

## Why video games?

This question should answer itself.

The first think I did on a computer was play a video game.

The first thing I wanted to write when I started programming was a video game.

And I still do both

More preactically:

1. They are attractive for newcomers

2. Programming them is _real hard_

Which is another way of saying: "they are complex, and pose really interesting questions"

## Programming Patterns

Online book: http://gameprogrammingpatterns.com/

Go read it

### Flyweight

For games with a grid, cells can be one of a number of tiles

Tiles are small and repeatable

Cells link to the tile, and specify positions

Easy to do with Moo

#### Examples in the wild?

Games::TMX::Parser does this

    package Tile {
        use Moo
        has sprite => ( is => 'ro', required => 1 );
        has props  => ( is => 'ro', default => sub { +{} } );
    };

    package Cell {
        use Moo;
        has tile => ( is => 'rw', weak_ref => 1 );
    };

    package Grid {
        use Moo;
        has cells => ( is => 'ro', sub
    };
