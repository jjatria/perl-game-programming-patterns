# Perl Games

## Libraries / Frameworks

### Low-level

#### Raylib

https://www.raylib.com

#### SDL

#### Term::Caca

#### Tickit

### Game engines

#### Arkess

https://github.com/konapun/arkess

#### Oyster

https://github.com/agmcc/Oyster

#### Roguelike::Utils

http://rll-pm.sourceforge.net/ [defunct]

Also: http://roguebasin.roguelikedevelopment.org/index.php?title=Roguelike_Library_For_Perl

And https://metacpan.org/release/Roguelike-Utils

#### Avenger

https://github.com/PerlGameDev/Avenger

#### GameFrame

https://github.com/eilara/GameFrame

#### GameFrame

https://github.com/dnmfarrell/March

## RPG

### The Lacuna Expanse (not active)

A browser-based space-exploration and civilization-building game, with an
espionage-focused combat system.

[Reported reaching 2000 users in the first two months][tle-interview].
Went offline [because of lack of funds][tle-offline] at the end of
September 2016.

The [server][tle-server] and [web-client][tle-client] code is available on
Github, as well as some of its [assets][tle-assets] and
[data files][tle-missions]. However, it is not trivial to set up a local
version of their server following their instructions.

Website: https://www.lacunaexpanse.com (not active)

[tle-missions]: https://github.com/plainblack/Lacuna-Missions
[tle-assets]: https://github.com/plainblack/Lacuna-Assets
[tle-client]: https://github.com/plainblack/Lacuna-Web-Client
[tle-server]: https://github.com/plainblack/Lacuna-Server-Open
[tle-interview]: https://www.onrpg.com/news/interview-how-expansive-is-lacuna-expanse/
[tle-offline]: https://www.facebook.com/lacunaexpanse/photos/a.10156303699310010/10157569562670010

### Kenó Antigen

A community-driven reboot of The Lacuna Expanse, still in the works
(although the last commit was about a year ago). Their code is also
[available on Github][kantigen], but running the game locally is likewise
not trivial.

Website: http://kenoantigen.com (not active... yet?)

### NationStates

Website: https://www.nationstates.net (_brimming_ with activity)

A browser-based nation simulation. The game was created by author
[Max Barry][] as a tie-in to one of his novels, but quickly took a life of its
own

The game has been running since November 2002.

They run community events like in the past year: [a zombie apocalypse][ns-zday]
and an [all-out nuclear war][ns-nuclear]

Fun fact: it received [a cease-and-desist letter][ns-unlegal] from the _actual_
United Nations to ask that they "cease and desist from using the United Nations
name and emblem" because "it may create the impression that your game is
related to the United Nations or is endorsed by it".

And of course, I have a nation too: nationstates.net/venthravene

[max barry]: https://maxbarry.com
[ns-unlegal]: https://www.nationstates.net/unlegal.pdf
[ns-zday]: https://www.nationstates.net/page=news/2018/10/27/index.html
[ns-nuclear]: https://www.nationstates.net/page=news/2018/09/23/index.html

## Action

### Microidium

Had to install

    * libxmu-dev

## Puzzle

### Frozen Bubble

In [the comments of the main executable][fb-comment]:

> `Yes it uses Perl, you non-believer :-)`

[fb-comment]: https://github.com/kthakore/frozen-bubble/blob/master/bin/frozen-bubble#L33

## Other

### Minesweeper

A [Tk-based Minesweeper clone][nids-web] by [Francesco Nidito][nids-web]
([NIDS][nids-cpan] on CPAN). Other than Tk, this single-file script does
not use any other modules.

It is written in a functional style, and the script is complete (although
there are some edge conditions that make the game hard to complete).

[nids-web]: http://www.di.unipi.it/~nids/
[nids-cpan]: https://metacpan.org/author/NIDS
